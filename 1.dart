import 'package:console_may2022/console_may2022.dart' as console_may2022;

void main(List<String> arguments) {
  String name = "Kagiso Seekoei", 
         favApp = "Youtube",
         city = "Bloemfontein";
  print('Hello! Your name is $name, your favourite app is $favApp, and you are from $city');
}
