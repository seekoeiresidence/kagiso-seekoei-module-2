import 'package:console_may2022/console_may2022.dart' as console_may2022;

void main(List<String> arguments) {
  var mtnApps = ['FNB Banking App', 'Discovery HealthID', 'TransUnioin Dealers Guide', 'RapidTargets', 'Matchy', 'Plascon Inspire Me', 'phraZapp',   //2012
                 'DStv', 'Telco Data Visualizer', 'PriceCheck Mobile', 'MarkitShare', 'Nedbank App suite', 'SnapScan', 'Kids Aid', 'Bookly', 'Gautrain Buddy', //2013
                 'SuperSport', 'SyncMobile', 'My Belongings', 'LIVE Inspect', 'Vigo', 'Zapper', 'Rea Vaya', 'Wildlife tracker',  //2014
                 'VulaMobile', 'DStv Now', 'WumDrop', 'CPUT Mobile', 'EskomSePush', 'M4JAM',  //2015
                 'iKhokha', 'HearZa', 'Tuta-me', 'KaChing', 'Friendly Math Monsters',         //2016
                 'Shyft', 'TransUnion 1Check', 'OrderIN', 'EcoSlips', 'InterGreatMe', 'Zulzi', 'Hey Jude', 'Pick n Pay Super Animals 2', 'The TreeApp South Africa', 'Watlf Health Portal', 'Awethu Project',  //2017
                 'Pineapple', 'Cowa Bunga', 'Digemy Knowledge Partner and Besmarter', 'Bestee', 'The African Cyber Gaming League App', 'dbTrack', 'Stokfella', 'Difela Hymns', 'Xander English', 'Ctrl', 'Khula ecosystem', 'ASI Snakes',   //2018
                 'Digger', 'SI Realities', 'Vula Mobile', 'Hydra Farm', 'Matric Live', 'Franc', 'Over', 'LocTransi', 'Naked Insurance', 'Loot Defence', 'moWash',  //2019
                 'EasyEquities', 'Examsta', 'CheckersSixty60', 'Technishen', 'BirdPro', 'Lexie Hearing app', 'GreenFingers Mobile', 'Xitsonga Dictionary', 'StokFella', 'Bottles',  //2020
                 'iiDENTIFii', 'Hellopay SoftPOS', 'Guardian Health Platform', 'Ambani Africa', 'Murimi', 'Shyft', 'Sisa', 'UniWise', 'Kazi', 'Takealot App', 'Rekindle Learning app', 'Roadsave', 'Afrihost'];  //2021

  String winner2017 = mtnApps[56], winner2018 = mtnApps[35];
  mtnApps.sort();
  int allApps = mtnApps.length;
  print('All Winning Apps: $mtnApps');
  print('Winning App of 2017: $winner2018 \nWinning app of 2018: $winner2017');
  print('Total number of apps: ${allApps}');
}
