import 'package:console_may2022/console_may2022.dart' as console_may2022;

void main(List<String> arguments) {
     var winnerInfo = Winner();
    
    print("App Name: ${winnerInfo.Capitalise(winnerInfo.appName)} \nCategory: ${winnerInfo.appCategory} \nDeveloper: ${winnerInfo.appDeveloper} \nWinning Year: ${winnerInfo.winningYear}");


}

class Winner {
  String appName = "Shyft";
  String appCategory = "Best Financial Solution";
  String appDeveloper = "Standard Bank";
  int winningYear = 2017;

  String Capitalise(capitaliseAppName) {
      return capitaliseAppName.toUpperCase();
  }
}
